package com.management.employees.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.sql.Timestamp;

@Getter
@AllArgsConstructor
public class Employee {

    private Long employeeId;
    private String name;
    private String position;
    private String phone;
    private Long taskId;
    private String taskDetail;
    private Timestamp regDate;
    private Timestamp modDate;
    private boolean deleted;

    @Builder
    public Employee(String name, String position, String phone, Long taskId, String taskDetail){
        this.name = name;
        this.position = position;
        this.phone = phone;
        this.taskId = taskId;
        this.taskDetail = taskDetail;
    }
}
