package com.management.employees.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Company {

    private long company_id;
    private String company_name;


}
