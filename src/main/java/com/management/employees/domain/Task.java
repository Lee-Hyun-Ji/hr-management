package com.management.employees.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Task {

    private long task_id;
    private String task_code;
    private String task_name;

}

