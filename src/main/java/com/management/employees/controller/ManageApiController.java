package com.management.employees.controller;

import com.management.employees.dto.manage.EmpCreateRequestDto;
import com.management.employees.dto.manage.EmpTaskUpdateRequestDto;
import com.management.employees.service.ManageService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
public class ManageApiController {

    @Autowired
    private ManageService service;

    /* Create API */
    @ApiOperation(value = "신규 사원 정보 등록"
            , notes = "신규 사원의 정보를 등록합니다.")
    @PostMapping("/manage/emp/new")
    public ResponseEntity createNewEmp(@RequestBody EmpCreateRequestDto requestDto) {
        return new ResponseEntity(HttpStatus.CREATED);
    }


    /* Read API */
    @ApiOperation(value = "사원 세부 정보 조회"
            , notes = "특정 ID의 사원 세부 정보를 조회합니다.")
    @ApiImplicitParam(name = "id", value = "멤버 ID", required = true)
    @GetMapping("/manage/emp/{id}")
    public ResponseEntity<HashMap<String, Object>> getEmpById(@PathVariable long id) {
        return new ResponseEntity<>(service.getEmpById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "전체 회사 리스트 제공"
            , notes = "전체 회사 리스트를 제공합니다.")
    @GetMapping("/manage/company/list")
    public ResponseEntity<HashMap<String, Object>> getCompanyList() {
        return new ResponseEntity<>(service.getCompanyList(), HttpStatus.OK);
    }

    @ApiOperation(value = "전체 업무 리스트 제공"
            , notes = "전체 회사 리스트를 제공합니다.")
    @GetMapping("/manage/task/list")
    public ResponseEntity<HashMap<String, Object>> getTaskList(){
        return new ResponseEntity<>(service.getTaskList(),HttpStatus.OK);
    }


    /* Update API */
    @ApiOperation(value = "사원 정보 수정(이름)"
            , notes = "사원의 이름 정보를 수정합니다.")
    @PostMapping("/manage/emp/{id}/name")
    public ResponseEntity<HashMap<String, Object>> updateEmpName(@PathVariable long id, @RequestBody String newName) {
        return new ResponseEntity<>(service.updateEmpName(id, newName),HttpStatus.OK);
    }

    @ApiOperation(value = "사원 정보 수정(직급)"
            , notes = "사원의 직급 정보를 수정합니다.")
    @PostMapping("/manage/emp/{id}/position")
    public ResponseEntity<HashMap<String, Object>> updateEmpPosition(@PathVariable long id, @RequestBody String newPosition) {
        return new ResponseEntity<>(service.updateEmpPosition(id, newPosition),HttpStatus.OK);
    }

    @ApiOperation(value = "사원 정보 수정(전화번호)"
            , notes = "사원의 전화번호 정보를 수정합니다.")
    @PostMapping("/manage/emp/{id}/phone")
    public ResponseEntity<HashMap<String, Object>> updateEmpPhone(@PathVariable long id, @RequestBody String newPhone) {
        return new ResponseEntity<>(service.updateEmpPhone(id, newPhone),HttpStatus.OK);
    }

    @ApiOperation(value = "사원 정보 수정(업무)"
            , notes = "사원의 업무 정보를 수정합니다.")
    @PostMapping("/manage/emp/{id}/task")
    public ResponseEntity<HashMap<String, Object>> updateEmpPhone(@PathVariable long id, @RequestBody EmpTaskUpdateRequestDto requestDto) {
        return new ResponseEntity<>(service.updateEmpTask(id, requestDto),HttpStatus.OK);
    }


    /* Delete API */
    @ApiOperation(value = "사원 정보 삭제"
            , notes = "특정 ID의 사원 정보를 삭제합니다.")
    @GetMapping("/manage/emp/delete/{id}")
    public ResponseEntity<HashMap<String, Object>> deleteEmpInfo(@PathVariable long id) {
        return new ResponseEntity<>(service.deleteEmp(id),HttpStatus.OK);
    }


}
