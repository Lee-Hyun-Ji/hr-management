package com.management.employees.controller;


import com.management.employees.service.HomeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
public class HomeApiController {

    @Autowired
    private HomeService service;

    @ApiOperation(value = "전체 리스트 조회"
            , notes = "전체 사원 리스트를 조회합니다.")
    @GetMapping("/home/list")
    public ResponseEntity<HashMap<String, Object>> getEmpList(){
        return new ResponseEntity<>(service.getEmpList(), HttpStatus.OK);
    }


    @ApiOperation(value = "사원 이름으로 검색"
            , notes = "사원 이름을 키워드로 검색합니다.")
    @GetMapping("/home/list/search")
    public ResponseEntity<HashMap<String, Object>> searchByName(@RequestParam String keyword){
        return new ResponseEntity<>(service.searchByEmpName(keyword),HttpStatus.OK);
    }

}
