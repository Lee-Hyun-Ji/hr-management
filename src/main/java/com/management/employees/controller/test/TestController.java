/* package com.management.employees.controller;

import com.management.employees.service.EmployeeService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
public class TestController {

    @Autowired
    private EmployeeService service;

    @ApiOperation(value = "Test API - 전체 멤버 조회"
                , notes = "전체 멤버 정보를 조회합니다.")
    @GetMapping("/test-api/members")
    public HashMap<String, Object> getAllMembers(){
        return service.getAllMembers();
    }

    @ApiOperation(value = "Test API - 특정 멤버 조회"
            , notes = "Id를 통해 특정 멤버의 정보를 조회합니다.")
    @ApiImplicitParam(name = "id", value = "멤버 ID", required = true)
    @GetMapping("/test-api/members/{id}")
    public HashMap<String, Object> getMemberById(@PathVariable int id){
        return service.getMemberById(id);
    }

}
*/