package com.management.employees.dto.home;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class EmpListResponseDto {

    private Long employeeId;
    private List<String> company;
    private String name;
    private String position;
    private String task;

    @Builder
    public EmpListResponseDto(Long employeeId, List<String> company, String name, String position, String task) {
        this.employeeId = employeeId;
        this.company = company;
        this.name = name;
        this.position = position;
        this.task = task;
        this.task = task;
    }

}
