package com.management.employees.dto.test;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class TestMemberDto {

    private int id;
    private String name;
    private String email;
    private String phone;

}
