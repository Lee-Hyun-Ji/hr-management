package com.management.employees.dto.manage;

import com.management.employees.domain.Employee;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class EmpInfoResponseDto {

    private String name;
    private String position;
    private List<String> companyName;
    private String phone;
    private String TaskName;
    private String taskDetail;

    @Builder
    public EmpInfoResponseDto(Employee employee, String taskName, List<String> companyName){
        this.name = employee.getName();
        this.position = employee.getPosition();
        this.phone = employee.getPhone();
        this.taskDetail = employee.getTaskDetail();
        this.companyName = companyName;
        this.TaskName = taskName;
    }
}
