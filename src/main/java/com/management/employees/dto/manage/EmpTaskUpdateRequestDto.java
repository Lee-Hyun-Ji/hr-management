package com.management.employees.dto.manage;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class EmpTaskUpdateRequestDto {

    private Long taskId;
    private String taskDetail;

}
