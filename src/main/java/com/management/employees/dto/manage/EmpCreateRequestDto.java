package com.management.employees.dto.manage;

import com.management.employees.domain.Employee;
import lombok.*;

@Getter
@AllArgsConstructor
public class EmpCreateRequestDto {

    private String name;
    private String position;
    private long[] companyId;
    private String phone;
    private long taskId;
    private String taskDetail;

    public Employee toEntity(){
        return Employee.builder()
                .name(name)
                .position(position)
                .phone(phone)
                .taskId(taskId)
                .taskDetail(taskDetail)
                .build();
    }
}
