package com.management.employees.service;

import java.util.HashMap;

public interface HomeService {
    public HashMap<String, Object> getEmpList();
    public HashMap<String, Object> searchByEmpName(String keyword);
}
