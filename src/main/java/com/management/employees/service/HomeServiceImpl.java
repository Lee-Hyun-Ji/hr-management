package com.management.employees.service;

import com.management.employees.dto.home.EmpListResponseDto;
import com.management.employees.mapper.CompanyMapper;
import com.management.employees.mapper.EmployeeMapper;
import com.management.employees.mapper.JoinMapper;
import com.management.employees.mapper.TaskMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HomeServiceImpl implements HomeService{

    @Autowired
    private EmployeeMapper employeeMapper;
    @Autowired
    private JoinMapper joinMapper;
    @Autowired
    private CompanyMapper companyMapper;
    @Autowired
    private TaskMapper taskMapper;

    @Override
    @Transactional
    public HashMap<String, Object> getEmpList() {
        HashMap<String, Object> retVal = new HashMap<>();

        List<EmpListResponseDto> list = employeeMapper.selectAll().stream()
                .map(emp -> EmpListResponseDto.builder()
                        .employeeId(emp.getEmployeeId())
                        .company(joinMapper.selectCompanyNameByEmpId(emp.getEmployeeId()))
                        .name(emp.getName())
                        .position(emp.getPosition())
                        .task(taskMapper.selectById(emp.getTaskId()).getTask_name())
                        .build())
                .collect(Collectors.toList());

        retVal.put("totCnt", list.size());
        retVal.put("data", list);
        return retVal;
    }


    @Override
    @Transactional
    public HashMap<String, Object> searchByEmpName(String keyword) {
        HashMap<String, Object> retVal = new HashMap<>();

        List<EmpListResponseDto> list = employeeMapper.searchByEmpName(keyword).stream()
                .map(emp -> EmpListResponseDto.builder()
                        .employeeId(emp.getEmployeeId())
                        .company(joinMapper.selectCompanyNameByEmpId(emp.getEmployeeId()))
                        .name(emp.getName())
                        .position(emp.getPosition())
                        .task(taskMapper.selectById(emp.getTaskId()).getTask_name())
                        .build())
                .collect(Collectors.toList());

        retVal.put("keyword", keyword);
        retVal.put("totCnt", list.size());
        retVal.put("data", list);
        return retVal;
    }
}
