package com.management.employees.service;

import com.management.employees.dto.manage.EmpCreateRequestDto;
import com.management.employees.dto.manage.EmpInfoResponseDto;
import com.management.employees.dto.manage.EmpTaskUpdateRequestDto;

import java.util.HashMap;

public interface ManageService {

    public void createNewEmp(EmpCreateRequestDto requestDto);

    public HashMap<String, Object> getEmpById(long id);
    public HashMap<String, Object> getCompanyList();
    public HashMap<String, Object> getTaskList();

    public HashMap<String, Object> updateEmpName(long id, String newName);
    public HashMap<String, Object> updateEmpPosition(long id, String newPosition);
    public HashMap<String, Object> updateEmpPhone(long id, String newPhone);
    public HashMap<String, Object> updateEmpTask(long id, EmpTaskUpdateRequestDto requestDto);

    public HashMap<String, Object> deleteEmp(long id);


}
