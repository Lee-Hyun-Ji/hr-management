package com.management.employees.service;

import com.management.employees.domain.Company;
import com.management.employees.domain.Employee;
import com.management.employees.domain.Task;
import com.management.employees.dto.manage.EmpCreateRequestDto;
import com.management.employees.dto.manage.EmpInfoResponseDto;
import com.management.employees.dto.manage.EmpTaskUpdateRequestDto;
import com.management.employees.mapper.CompanyMapper;
import com.management.employees.mapper.EmployeeMapper;
import com.management.employees.mapper.JoinMapper;
import com.management.employees.mapper.TaskMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ManageServiceImpl implements ManageService{

    @Autowired
    private EmployeeMapper employeeMapper;
    @Autowired
    private JoinMapper joinMapper;
    @Autowired
    private CompanyMapper companyMapper;
    @Autowired
    private TaskMapper taskMapper;

    /* Create API */
    @Override
    @Transactional
    public void createNewEmp(EmpCreateRequestDto requestDto) {
        Employee newEmp = requestDto.toEntity();
        long[] companyIdList = requestDto.getCompanyId();

        employeeMapper.insertNewEmployee(newEmp);
        for(long companyId: companyIdList){
            joinMapper.insertNewJoinInfo(newEmp.getEmployeeId(), companyId);
        }
    }

    /* Read API */
    @Override
    @Transactional
    public HashMap<String, Object> getEmpById(long id) {
        HashMap<String, Object> retVal = new HashMap<>();

        try {
            Employee emp = employeeMapper.selectById(id);
            String taskName = taskMapper.selectById(emp.getTaskId()).getTask_name();
            List<String> companyName = joinMapper.selectCompanyNameByEmpId(emp.getEmployeeId());
            EmpInfoResponseDto responseDto = EmpInfoResponseDto.builder()
                                             .employee(emp).taskName(taskName).companyName(companyName).build();
            retVal.put("data", responseDto);

        } catch (NullPointerException npe){
            retVal.put("data", "해당 ID의 사원이 존재하지 않습니다.");
        }

        return retVal;
    }

    @Override
    @Transactional
    public HashMap<String, Object> getCompanyList() {
        HashMap<String, Object> retVal = new HashMap<>();

        List<Company> list = companyMapper.selectAllCompany();
        retVal.put("totCnt",list.size());
        retVal.put("data", list);

        return retVal;
    }

    @Override
    @Transactional
    public HashMap<String, Object> getTaskList() {
        HashMap<String, Object> retVal = new HashMap<>();

        List<Task> list = taskMapper.selectAllTask();
        retVal.put("totCnt",list.size());
        retVal.put("data", list);

        return retVal;
    }


    /* Update API */
    @Override
    @Transactional
    public HashMap<String, Object> updateEmpName(long id, String newName) {
        HashMap<String, Object> retVal = new HashMap<>();

        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        map.put("name", newName);
        int res = employeeMapper.updateEmpName(map);

        if(res == 1){
            retVal.put("is_success", true);
        } else {
            retVal.put("is_success", false);
        }

        return retVal;
    }

    @Override
    @Transactional
    public HashMap<String, Object> updateEmpPosition(long id, String newPosition) {
        HashMap<String, Object> retVal = new HashMap<>();

        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        map.put("position", newPosition);
        int res = employeeMapper.updateEmpPosition(map);

        if(res == 1){
            retVal.put("is_success", true);
        } else {
            retVal.put("is_success", false);
        }

        return retVal;
    }

    @Override
    @Transactional
    public HashMap<String, Object> updateEmpPhone(long id, String newPhone) {
        HashMap<String, Object> retVal = new HashMap<>();

        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        map.put("phone", newPhone);
        int res = employeeMapper.updateEmpPhone(map);

        if(res == 1){
            retVal.put("is_success", true);
        } else {
            retVal.put("is_success", false);
        }

        return retVal;
    }

    @Override
    @Transactional
    public HashMap<String, Object> updateEmpTask(long id, EmpTaskUpdateRequestDto requestDto) {
        HashMap<String, Object> retVal = new HashMap<>();

        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        map.put("taskId", requestDto.getTaskId());
        map.put("taskDetail", requestDto.getTaskDetail());
        int res = employeeMapper.updateEmpTask(map);

        if(res == 1){
            retVal.put("is_success", true);
        } else {
            retVal.put("is_success", false);
        }

        return retVal;
    }


    /* Delete API */
    @Override
    @Transactional
    public HashMap<String, Object> deleteEmp(long id) {
        HashMap<String, Object> retVal = new HashMap<>();

        int res = employeeMapper.deleteEmp(id);

        if(res == 1){
            retVal.put("is_success", true);
        } else {
            retVal.put("is_success", false);
        }

        return retVal;
    }

}
