/* package com.management.employees.service;

import com.management.employees.dto.test.TestMemberDto;
import com.management.employees.mapper.test.TestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService{

    @Autowired
    private TestMapper mapper;

    @Override
    public HashMap<String, Object> getAllMembers() {
        HashMap<String, Object> retVal = new HashMap<>();
        List<TestMemberDto> memberList = mapper.selectAllMembers();

        retVal.put("totCnt", memberList.size());
        retVal.put("data", memberList);

        return retVal;
    }

    @Override
    public HashMap<String, Object> getMemberById(int id) {
        HashMap<String, Object> retVal = new HashMap<>();

        retVal.put("requestId",id);
        retVal.put("data", mapper.selectMemberById(id));

        return retVal;
    }


}
*/