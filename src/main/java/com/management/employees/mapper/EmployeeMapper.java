package com.management.employees.mapper;

import com.management.employees.domain.Employee;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface EmployeeMapper {
    //HomeController
    public List<Employee> selectAll();
    public List<Employee> searchByEmpName(String keyword);

    //ManageController
    public int insertNewEmployee(Employee employee);
    public Employee selectById(long id);
    public int updateEmpName(Map map);
    public int updateEmpPosition(Map map);
    public int updateEmpPhone(Map map);
    public int updateEmpTask(Map map);
    public int deleteEmp(long id);
}
