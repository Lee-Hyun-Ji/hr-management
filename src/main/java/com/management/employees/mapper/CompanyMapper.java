package com.management.employees.mapper;

import com.management.employees.domain.Company;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CompanyMapper {
    //ManageController
    public Company findById(long id);
    public List<Company> selectAllCompany();
}
