package com.management.employees.mapper;


import com.management.employees.domain.Task;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TaskMapper {
    //HomeController, ManageController
    public Task selectById(long id);

    //ManageController
    public List<Task> selectAllTask();

}
