package com.management.employees.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface JoinMapper {
    //HomeController
    public List<String> selectCompanyNameByEmpId(long empId);

    //ManageController
    public int insertNewJoinInfo(long employeeId, long companyId);
}
